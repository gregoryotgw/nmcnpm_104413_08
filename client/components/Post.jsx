/** @jsx React.DOM */

var PointsBanner = require('./PointsBanner.jsx');
var UpvoteButton = require('./UpvoteButton.jsx')
var helpers = require('../helper-functions.js');
var React = require('react');

var Post = React.createClass({
    propTypes: {
        handleUpvote: React.PropTypes.func,
        handleRemoveUpvote: React.PropTypes.func,
        handleDeletePost: React.PropTypes.func,
        post: React.PropTypes.shape({
            id: React.PropTypes.string,
            author: React.PropTypes.string,
            title: React.PropTypes.string,
            url: React.PropTypes.string,
            self_text: React.PropTypes.string,
            score: React.PropTypes.number,
            subpy: React.PropTypes.string,
            upvoted: React.PropTypes.bool,
            comment_count: React.PropTypes.number,
            age: React.PropTypes.object
        }),
        upvoteImageURL: React.PropTypes.string.isRequired,
        upvotedImageURL: React.PropTypes.string.isRequired,
    },
    render: function() {
        var post = this.props.post;

        var UpvoteButtonProps = {
            upvoted: post.upvoted,
            onUpvote: this.props.handleUpvote,
            onRemoveUpvote: this.props.handleRemoveUpvote,
            upvoteImageURL: this.props.upvoteImageURL,
            upvotedImageURL: this.props.upvotedImageURL,
            targetId: post.id,
            defaultClassName: "post-upvote-button"
        };

        var PostInfoBannerProps = {
            score: post.score,
            age: post.age,
            author: post.author,
            upvoted: post.upvoted,
            commentCount: post.comment_count,
            postId: post.id,
            subpy: post.subpy,
            deletePost: this.props.handleDeletePost,
            user: this.props.user,
        }

        var selfTextBanner = '';
        if (post.self_text) {
            selfTextBanner = <p className="post-self-text-banner">{post.self_text}</p>
        }
        return (
            <li className="post">
                <UpvoteButton {...UpvoteButtonProps} />
                <PostTitle post={post} />
                {selfTextBanner}
                <PostInfoBanner {...PostInfoBannerProps} />
            </li>
        );
    }
});

var PostTitle = React.createClass({
    render: function() {
        var post = this.props.post;
        var postURL = post.url;
        var commentsURL = '/r/' + post.subpy + '/comments/' + post.id; 
        if (postURL === null) {
            postURL = commentsURL;
            return (
                <div className="post-title">
                    <a href={postURL} className="post-title-text">{post.title}</a>
                </div>
            );
        } else {
            if (postURL.search("http") == -1) {
                postURL = '//' + postURL;        
            }
            postURLShow = postURL.slice(postURL.search("//") + 2);
            return (
                <div className="post-title">
                    <a href={postURL} className="post-title-text" target="_blank">{post.title}</a>&nbsp; 
                    <a className="post-title-link">({postURLShow})</a>
                </div>
            );
        }
    }
});

var PostInfoBanner = React.createClass({
    render: function() {
        var bannerAge = helpers.getAgeString(this.props.age);
        var authorURL = '/u/' + this.props.author;
        var commentString = helpers.getCommentCountString(this.props.commentCount);
        var deletePost = this.props.deletePost;
        var commentsURL = '/r/' + this.props.subpy + '/comments/' + this.props.postId; 

        return (
            <div className="post-info-banner">
                <p>
                    <PointsBanner
                        upvoted={this.props.upvoted}
                        score={this.props.score}
                        defaultClassName="post-points-banner"
                    />
                     • Submitted by <a href={authorURL} className="post-author">{this.props.author}</a> <span className="post-age">{bannerAge}</span> ago • <a href={commentsURL}>{commentString}</a> • <a className="post-delete-button" onClick={deletePost.bind(null, this.props.postId)}>delete</a>
                </p>
            </div>
        );
    }
});

module.exports = Post;
