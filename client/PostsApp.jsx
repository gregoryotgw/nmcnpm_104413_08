// Handle pass in data
var scriptDOM = document.getElementById('postsAppScript');
var subpy = scriptDOM.getAttribute('subpy');
var username = scriptDOM.getAttribute('username');
var sortBy = scriptDOM.getAttribute('sortBy');

//Routes
var defaultURL;

var config = require('../config');
var helpers = require('./helper-functions');
var Post = require('./components/Post.jsx');
var UpvoteButton = require('./components/UpvoteButton.jsx');
var PointsBanner = require('./components/PointsBanner.jsx');
var React = require('react');

if(sortBy==null) {
    sortBy = 'new';
}
if(subpy==null) {
    defaultURL = '/posts/u/' + username + '?age=alltime&sort_by=' + sortBy;
} else {
    defaultURL = '/posts/r/' + subpy + '?age=alltime&sort_by=' + sortBy;
}
console.log(sortBy);
var App = React.createClass({
    getInitialState: function() {
        return {
            posts: [],
            upvotePool: {},
        };
    },
    updateUpvotePool: function(postId, isUpvoting) {
        var newUpvotePool = this.state.upvotePool;
        newUpvotePool[postId] = isUpvoting;
        this.setState({upvotePool:newUpvotePool});
    },
    updatePostsAfterUpvote: function(postId, isUpvoting) {
        var updatedPosts = helpers.findAndUpdateUpvoted(this.state.posts, postId, isUpvoting);
        this.setState({posts: updatedPosts});
    },
    updateAfterUpvote: function(postId, isUpvoting) {
        this.updatePostsAfterUpvote(postId, isUpvoting);
        this.updateUpvotePool(postId, isUpvoting);

        var ajaxURL = '/posts/'
        if (isUpvoting) {
            ajaxURL += 'upvote/' + postId;
        } else {
            ajaxURL += 'remove-upvote/' + postId;
        }

        $.ajax({
            type: 'POST',
            url: ajaxURL
        });
    },
    handleUpvote: function(postId) {
        this.updateAfterUpvote(postId, true);
    },
    handleRemoveUpvote: function(postId) {
        this.updateAfterUpvote(postId, false);
    },
    handleDeletePost: function(postId) {
        $.ajax({
            url: '/posts/delete/' + postId,
            type: 'POST',
            success: function() {
                this.loadPostsFromServer();
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    loadPostsFromServer: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            success: function(data) {
                for(var i = 0; i<Object.keys(data).length; i++) {
                    if(data[i].self_text!=null && data[i].self_text.length > 205) {
                        data[i].self_text = data[i].self_text.substr(0,200) + '...'; 
                    }
                    if(data[i].title!=null && data[i].title.length > 85) {
                        data[i].title = data[i].title.substr(0,80) + '...';
                    }
                }
                this.setState({posts: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    componentDidMount: function() {
        this.loadPostsFromServer();
        setInterval(this.loadPostsFromServer, this.props.pollInterval);
    },
    render: function() {
        //console.log(this.state.posts);
        return (
            <div className="posts-app">
                <PostsList
                    handleUpvote={this.handleUpvote}
                    handleRemoveUpvote={this.handleRemoveUpvote}
                    handleDeletePost={this.handleDeletePost}
                    posts={this.state.posts}
                    upvoteImageURL={this.props.upvoteImageURL}
                    upvotedImageURL={this.props.upvotedImageURL}
                />
            </div>
        );
    }
});

var PostsList = React.createClass({
    render: function() {
        var props = this.props;

        var posts = this.props.posts.map(function(post) {
            return (
                <Post
                    handleUpvote={props.handleUpvote}
                    handleRemoveUpvote={props.handleRemoveUpvote}
                    handleDeletePost={props.handleDeletePost}
                    post={post}
                    upvoteImageURL={props.upvoteImageURL}
                    upvotedImageURL={props.upvotedImageURL}
                />
            );
        });

        return (
            <ul className="postsList">
                {posts}
            </ul>
        );
    }
});


React.render(
    <App
        url={defaultURL}
        pollInterval={config.postPollInterval}
        upvoteImageURL={"/images/upvote.svg"}
        upvotedImageURL={"/images/upvoted.svg"}
    />,
    document.getElementById("react-posts-app-mount")
);
